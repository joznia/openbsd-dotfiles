{-
    (_)___ ____ _ (_)__ _
    | / _ \_ / ' \| / _` |
   _/ \___/__|_||_|_\__,_|
  |__/

  Joznia's xmonad config
  Based on Darcs, some aspects were taken from DistroTube's config (e.g. the layouts)
  Aims to be minimal, yet sexy and powerful.

-}

-- Imports
import XMonad
import Data.Monoid
import System.Exit
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO
import XMonad.Actions.MouseResize
import XMonad.Util.SpawnOnce
import XMonad.Layout.Gaps
import XMonad.Layout.Tabbed
import XMonad.Actions.SinkAll
import XMonad.Hooks.ManageHelpers 
import XMonad.Layout.GridVariants (Grid(Grid))
import XMonad.Layout.SimplestFloat
import XMonad.Layout.Spiral
import XMonad.Layout.ResizableTile
import XMonad.Layout.ThreeColumns
import XMonad.Layout.LayoutModifier
import XMonad.Layout.LimitWindows (limitWindows, increaseLimit, decreaseLimit)
import XMonad.Layout.Magnifier
import XMonad.Layout.MultiToggle (mkToggle, single, EOT(EOT), (??))
import XMonad.Layout.MultiToggle.Instances (StdTransformers(NBFULL, MIRROR, NOBORDERS))
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ShowWName
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.SubLayouts
import XMonad.Layout.WindowNavigation
import XMonad.Layout.WindowArranger (windowArrange, WindowArrangerMsg(..))
import qualified XMonad.Layout.ToggleLayouts as T (toggleLayouts, ToggleLayout(Toggle))
import qualified XMonad.Layout.MultiToggle as MT (Toggle(..))
import XMonad.Actions.SinkAll
import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- Preferred programs
myTerminal      = "lxterminal"
myFileManager   = "ranger"
myEditor        = "nvim"
myRunLauncher   = "rofi -combi-modi window,drun,ssh -theme Arc-Dark -font \"Ubuntu Mono 16\" -show combi -icon-theme \"Papirus Dark\" -show-icons"
myBrowser       = "firefox"
myIrcClient     = "irssi"
myScreenshotter = "xfce4-screenshooter"
mySessionMgr    = "/bin/bash /home/reno/.local/bin/session"

-- Font
myFont :: String
myFont = "xft:Ubuntu:weight=bold:size=11:antialias=true:hinting=true"

-- Should focus follow the pointer?
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Should a click on the window to focus pass a click to the window?
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Window border width
myBorderWidth   = 3

-- Set mod key to Windows key
myModMask       = mod4Mask

-- Name of workspaces
myWorkspaces    = ["term","www","edit","vid","virt","gfx","obs","chat","mus"]

-- Layouts
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) True (Border i i i i) True

tall     = renamed [Replace "tall"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ ResizableTall 1 (3/100) (1/2) []
--magnify  = renamed [Replace "magnify"]
--         $ windowNavigation
--         $ addTabs shrinkText myTabTheme
--         $ subLayout [] (smartBorders Simplest)
--         $ magnifier
--         $ limitWindows 12
--         $ mySpacing 8
--         $ ResizableTall 1 (3/100) (1/2) []
monocle  = renamed [Replace "monocle"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 Full
floats   = renamed [Replace "floats"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 20 simplestFloat
grid     = renamed [Replace "grid"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ limitWindows 12
           $ mySpacing 8
           $ mkToggle (single MIRROR)
           $ Grid (16/10)
spirals  = renamed [Replace "spirals"]
           $ windowNavigation
           $ addTabs shrinkText myTabTheme
           $ subLayout [] (smartBorders Simplest)
           $ mySpacing 8
           $ spiral (6/7)
--threeCol = renamed [Replace "threeCol"]
--         $ windowNavigation
--         $ addTabs shrinkText myTabTheme
--         $ subLayout [] (smartBorders Simplest)
--         $ limitWindows 7
--         $ mySpacing 4
--         $ ThreeCol 1 (3/100) (1/2)
--threeRow = renamed [Replace "threeRow"]
--         $ windowNavigation
--         $ addTabs shrinkText myTabTheme
--         $ subLayout [] (smartBorders Simplest)
--         $ limitWindows 7
--         $ mySpacing 4
--         -- Mirror takes a layout and rotates it by 90 degrees.
--         -- So we are applying Mirror to the ThreeCol layout.
--         $ Mirror
--         $ ThreeCol 1 (3/100) (1/2)
tabs     = renamed [Replace "tabs"]
           -- I cannot add spacing to this layout because it will
           -- add spacing between window and tabs which looks bad.
           $ tabbed shrinkText myTabTheme

-- Border colors
myNormalBorderColor  = "#1d252c"
myFocusedBorderColor = "#5ec4ff"

-- Colors
myTabTheme = def { fontName            = myFont
                 , activeColor         = "#5ec4ff"
                 , inactiveColor       = "#1d252c"
                 , activeBorderColor   = "#5ec4ff"
                 , inactiveBorderColor = "#1d152d"
                 , activeTextColor     = "#282c34"
                 , inactiveTextColor   = "#d0d0d0"
                 }

-- Theme for showWName which prints current workspace when you change workspaces.
myShowWNameTheme :: SWNConfig
myShowWNameTheme = def
    { swn_font              = "xft:Ubuntu:bold:size=60"
    , swn_fade              = 1.0
    , swn_bgcolor           = "#1c1f24"
    , swn_color             = "#ffffff"
    }

-- the layout hook
myLayoutHook = avoidStruts $ myDefaultLayout
             where
               myDefaultLayout =     tall
                                 ||| noBorders monocle
                                 ||| floats
                                 ||| noBorders tabs
                                 ||| grid
                                 ||| spirals

-- Window rules: windows with doFloat will float by default, and doShift moves them to a specific workspace by default.
myManageHook = composeAll [
      manageDocks
    , isFullscreen --> doFullFloat
    , className =? "Gimp-2.10"           --> doFloat >> doShift "gfx"
    , className =? "Virt-manager"        --> doFloat >> doShift "virt"
    , className =? "obs"                 --> doFloat >> doShift "obs"
    , className =? "vlc"                 --> doFloat >> doShift "vid"
    , className =? "Emacs"               --> doFloat >> doShift "edit"
    , className =? "discord"             --> doFloat >> doShift "chat"
    , className =? "zoom"                --> doFloat >> doShift "chat"
    , className =? "firefox"             --> doShift "www"
    , className =? "TeamViewer"          --> doFloat
    , className =? "plasmashell"         --> doFloat
    , className =? "feh"                 --> doFloat
    , className =? "Xmessage"            --> doFloat
    , className =? "Xfce4-screenshooter" --> doFloat
    , resource  =? "desktop_window"      --> doIgnore
    , resource  =? "kdesktop"            --> doIgnore
    ]

-- Event handling
myEventHook = mempty

-- Log hook
--myLogHook = return ()

-- Launch programs on startup
myStartupHook = do
        spawnOnce "picom -b &"
        spawnOnce "nitrogen --restore &"
        spawnOnce "discord &"
        spawnOnce "xrandr --output DP-1 --mode 1920x1080 --rate 120 --primary --left-of DP-3 --output DP-3 --mode 1920x1080 --rate 120 &"
        spawnOnce "xsetroot -cursor_name left_ptr &"
        spawnOnce "xrdb ~/.Xresources &"
        spawnOnce "lxsession &"

-- Keybinds
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm                , xK_Return), spawn (myTerminal))                                     -- terminal
    , ((modm .|. shiftMask  , xK_Return), spawn (myRunLauncher))                                  -- program launcher
    , ((modm .|. shiftMask  , xK_z     ), spawn "kill -9 $(pgrep xmobar)")                        -- kill xmobar
    , ((modm .|. controlMask, xK_v     ), spawn (myTerminal ++ " -e " ++ myEditor))               -- vim
    , ((modm .|. controlMask, xK_f     ), spawn (myTerminal ++ " -e " ++ myFileManager))          -- file manager
    , ((modm .|. controlMask, xK_i     ), spawn (myTerminal ++ " -e " ++ myIrcClient))            -- IRC chat
    , ((modm .|. controlMask, xK_e     ), spawn "emacsclient -c")                                 -- Emacs
    , ((modm .|. controlMask, xK_w     ), spawn (myBrowser))                                      -- web browser
    , ((modm .|. shiftMask  , xK_s     ), spawn (myScreenshotter))                                -- screenshot
    , ((modm .|. shiftMask  , xK_c     ), kill)                                                   -- close focused window
    , ((modm .|. shiftMask  , xK_x     ), sinkAll)                                                -- push all windows back into tiling
    , ((modm                , xK_space ), sendMessage NextLayout)                                 -- cycle layouts
    , ((modm .|. shiftMask  , xK_space ), setLayout $ XMonad.layoutHook conf)                     -- reset layout to default
    , ((modm                , xK_n     ), refresh)                                                -- resize windows to correct size
    , ((modm                , xK_j     ), windows W.focusDown)                                    -- change focus to the next window
    , ((modm                , xK_k     ), windows W.focusUp  )                                    -- change focus to previous window
    , ((modm                , xK_m     ), windows W.focusMaster  )                                -- change focus to master
    , ((modm                , xK_Tab   ), windows W.swapMaster)                                   -- swap current focused window w/ master
    , ((modm .|. shiftMask  , xK_j     ), windows W.swapDown  )                                   -- swap current window w/ next window
    , ((modm .|. shiftMask  , xK_k     ), windows W.swapUp    )                                   -- swap current window w/ previous window
    , ((modm                , xK_h     ), sendMessage Shrink)                                     -- shrink master window
    , ((modm                , xK_l     ), sendMessage Expand)                                     -- enlarge master window
    , ((modm                , xK_t     ), withFocused $ windows . W.sink)                         -- push a window back into tiling
    , ((modm                , xK_comma ), sendMessage (IncMasterN 1))                             -- increase windows in master
    , ((modm                , xK_period), sendMessage (IncMasterN (-1)))                          -- decrease windows in master
    , ((modm                , xK_b     ), sendMessage ToggleStruts)                               -- toggle status bar gap
    , ((modm .|. mod1Mask   , xK_q     ), spawn (myTerminal ++ " -e " ++ mySessionMgr))           -- start session manager
    , ((modm .|. shiftMask  , xK_q     ), spawn "xmonad --recompile; xmonad --restart")           -- restart xmonad
   
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

-- Run xmonad with our defaults
main = do
  -- Run xmobar
  xmproc0 <- spawnPipe "xmobar -x 0 /home/reno/.config/xmobar/xmobarrc0"
  xmproc1 <- spawnPipe "xmobar -x 1 /home/reno/.config/xmobar/xmobarrc1"
  -- Finally, run xmonad
  xmonad $ docks def
      {
      -- simple stuff
        focusFollowsMouse  = myFocusFollowsMouse
      , clickJustFocuses   = myClickJustFocuses
      , borderWidth        = myBorderWidth
      , modMask            = myModMask
      , workspaces         = myWorkspaces
      , normalBorderColor  = myNormalBorderColor
      , focusedBorderColor = myFocusedBorderColor

      -- key bindings
      , keys               = myKeys
      , mouseBindings      = myMouseBindings

      -- hooks, layouts
      , layoutHook         = myLayoutHook
      , manageHook         = myManageHook
      , handleEventHook    = myEventHook
      , logHook            = dynamicLogWithPP xmobarPP
                                { ppOutput = \x -> hPutStrLn xmproc0 x >> hPutStrLn xmproc1 x
                                , ppTitle = xmobarColor                 "#77eeee" "" . shorten 50               -- Current window title
                                , ppCurrent = xmobarColor               "#77eeee" "" . wrap " [" "] "           -- Current workspace
                                , ppVisible = xmobarColor               "#5ec4ff" "" . wrap " (" ") "           -- Visible workspaces (f.e. on other monitors)
                                , ppHidden = xmobarColor                "#5ec4ff" "" . wrap " " " "             -- Non-visible workspaces
                                , ppHiddenNoWindows = xmobarColor       "#5ec4ff" "" . wrap " " " "             -- Non-visible workspaces that also have no windows in them
                                , ppUrgent = xmobarColor                "#c45500" "" . wrap " !" "! "           -- Urgent workspace
                                , ppLayout = xmobarColor                "#8bd94c" ""
                                }
   -- , logHook            = myLogHook
      , startupHook        = myStartupHook
    }
